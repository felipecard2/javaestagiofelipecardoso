package candidatura.questao3;
import java.util.Random;

public class SomaDivisores {
public static void main(String[] args) {
		
		Random random = new Random();
		int R = random.nextInt(9) + 1;
		int S = random.nextInt(999) + 1;
		
		System.out.println("R = " + R);
		System.out.println("S = " + S);
		int soma = 0;
		
		for (int cont = 1; cont <= S; cont++) {
			if (cont % R == 0) {
				soma += cont;
				System.out.println(cont);
			}
		}
		System.out.println("Soma dos divisores de R: " + soma);
	}
}
