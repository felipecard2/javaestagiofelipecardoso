package candidatura.questao2;
import java.util.Random;

public class Randomico {
public static void main(String[] args) {
		
		Random random = new Random();
		int R = random.nextInt(9) + 1;
		int S = random.nextInt(99) + 1;
		
		System.out.println("R = " + R);
		System.out.println("S = " + S);
		
		int dobroR = R * 2; 
		
		for (int cont = 1; cont <= S; cont++) {
			if (cont % 2 == 0) {
				System.out.println(cont);
				if (cont % dobroR == 0) {
					System.out.println("Multiplo do dobro de R encontrado: " + cont);
					break;
				}
			}
		}
	}
}
