package candidatura.questao5;

public class Animais {
	boolean animal;
	boolean mamifero;
	boolean ave;
	
	public void comer() {
		System.out.println("Comer");
	}
	
	public void dormir() {
		System.out.println("Dormir");
	}
	
	public boolean mamar() {
		if(this.animal == mamifero) {
			System.out.println("Mamar");
			return true;
		} else {
			return false;
		}	
	}
	
	public boolean voar() {
		if(this.animal == ave) {
			System.out.println("Voar");
			return true;
		} else {
			return false;
		}	
	}
}
